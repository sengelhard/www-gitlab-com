---
layout: markdown_page
title: "Marketing Programs"
---


## On this page
{:.no_toc}

- TOC
{:toc}

## Marketing Programs

Marketing Programs focuses on executing, measuring and scaling GitLab's marketing programs such as email campaigns, event promotions, event follow up, drip email nurture series, webinars, and content. Marketing programs also aim to integrate data, personas and content to ensure relevant communications are delivered to the right person at the right time.

## Responsibilities within Marketing Programs function
<table>
  <tr>
    <th>Agnes Oetama</th>
    <th>Jackie Gragnola</th>
    <th>Jennifer Cordz (temporary until new lead routing rule is determined)</th>
  </tr>
  <tr>
    <td>Webcast Project Management/Set-up/Promotion/Follow-up, Ad-Hoc Emails (For Example - security alert emails, package/pricing changes), Bi-weekly newsletter review</td>
    <td>Field Event Support (Exclude List Upload), Gating Assets (not including on-demand webcasts), Drip email campaigns (email nurturing)</td>
    <td>List Upload from Field Events</td>
  </tr>
   <tr>
    <td>Ad-Hoc Emails (For Example - security alert emails, package/pricing changes)</td>
    <td>Drip email campaigns (email nurturing)</td>
    <td></td>
  </tr>
   <tr>
    <td>Bi-weekly newsletter review</td>
    <td>Gating Assets (not including on-demand webcasts)</td>
    <td></td>
  </tr>
</table>

Note - Each manager will also own process revamp (including issue template updates and email template design refresh) that falls within their area of resposibility.